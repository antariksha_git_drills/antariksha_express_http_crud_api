/* eslint-disable no-undef */
const { Sequelize, DataTypes } = require('sequelize');
require('dotenv').config();

const sequelize = new Sequelize(
  process.env.DATABASE,
  process.env.DB_USERNAME,
  process.env.DB_PASSWORD,
  {
    host: process.env.host,
    dialect: 'postgres',
  },
);

async function sequel() {
  try {
    await sequelize.authenticate();
    console.log('Connected database');
    const Todo = sequelize.define(
      'Todo',
      {
        id: {
          type: DataTypes.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        text: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        isCompleted: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
        },
      },
      {
        tableName: 'Todo',
        timestamps: false,
      },
    );
    await sequelize.sync({ alter: true });
    return Todo;
  } catch (err) {
    console.log(err);
  }
}

module.exports = sequel;
