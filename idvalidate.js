const idvalidation = (req, res, next) => {
  try {
    let valid = Number(req.params['id']);
    if (isNaN(valid)) {
      throw new Error('Not a valid id');
    }
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = idvalidation;
