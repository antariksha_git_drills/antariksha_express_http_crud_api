/* eslint-disable no-unused-vars */
const sequel = require('./sequelize.js');

const deleteData = async (req, res, next) => {
  try {
    const todo = await sequel();
    const data = await todo.destroy({
      where: {
        id: req.params['id'],
      },
    });
    const output = await todo.findAll();
    res.status(200).send(output);
  } catch (err) {
    next(err);
  }
};

module.exports = deleteData;
