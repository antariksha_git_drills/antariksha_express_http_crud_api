/* eslint-disable no-unused-vars */
const errMessage = {
  message: 'not found',
};
const errorHandle = (err, req, res, next) => {
  if (err.message == 'Id not found') {
    res.status(404).send(errMessage);
  } else if (err.message == 'Not a valid id') {
    res.status(400).send(err.message);
  } else {
    res.status(400).send(err.message);
  }
};

module.exports = errorHandle;
