/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
const validateId = require('./idvalidate.js');
require('dotenv').config();
const express = require('express');
const findId = require('./idfinder.js');
const sequel = require('./sequelize.js');
const errorHandle = require('./errorHandler.js');
const validateData = require('./objectValidate.js');
const inputData = require('./datainput.js');
const deleteData = require('./deleteData.js');

const app = express();

app.use(express.json());

app.get('/todos', async (req, res) => {
  const result = await sequel();
  const todos = await result.findAll();
  //console.log(JSON.stringify(todos, null, 2));
  res.json(todos);
});

app.get('/todos/:id', validateId, findId, async (req, res) => {
  const result = await sequel();
  const todos = await result.findAll({ where: { id: req.params['id'] } });
  res.json(todos);
});

app.post('/todos', validateData, async (req, res, next) => {
  try {
    const result = await sequel();
    const data = await result.create(req.body);
    res.send(data);
  } catch (err) {
    next(err);
  }
});

app.put('/todos/:id', validateId, findId, validateData, inputData);

app.delete('/todos/:id', validateId, findId, deleteData);

app.use(errorHandle);

app.listen(3000);
