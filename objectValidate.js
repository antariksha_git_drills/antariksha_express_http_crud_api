const yup = require('yup');

const yupObject = yup.object().shape({
  text: yup.string().required(),
  isCompleted: yup.boolean().required(),
});

const validateData = async (req, res, next) => {
  try {
    await yupObject.validate(req.body);
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = validateData;
