const sequel = require('./sequelize.js');
const findId = async (req, res, next) => {
  try {
    let id = Number(req.params['id']);
    const todo = await sequel();
    const todos = await todo.findAll();
    const findId = todos.find((index) => {
      return index.id == id;
    });
    if (typeof findId === 'undefined') {
      throw new Error('Id not found');
    } else {
      next();
    }
  } catch (err) {
    next(err);
  }
};

module.exports = findId;
