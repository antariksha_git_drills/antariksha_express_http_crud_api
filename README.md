# Antariksha_Express_HTTP_CRUD_API

## Install necessary packages

```
npm init
npm install
npm install express
npm install sequelize
npm install yup
```

## Import Functions

```
const express = require("express");
const { Sequelize, DataTypes } = require("sequelize");
const yup = require("yup");
```

## Create a database using sequelize

```
await sequelize.authenticate();
		console.log("Connected database");
		const Todo = sequelize.define(
			"Todo",
			{
				id: {
					type: DataTypes.INTEGER,
					autoIncrement: true,
					primaryKey: true,
				},
				text: {
					type: DataTypes.STRING,
					allowNull: false,
				},
				isCompleted: {
					type: DataTypes.BOOLEAN,
					allowNull: false,
				},
			},
			{
				tableName: "Todo",
				timestamps: false,
			},
		);
		await sequelize.sync({ alter: true });

```
## Create an .env file to store the necessary values to connect to the database and then import the env file

require("dotenv").config();

## Then connect it

```
const sequelize = new Sequelize(process.env.DATABASE, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
	host: process.env.host,
	dialect: "postgres",
});
```

## Create a yup object to validate the requested data

```
const yupObject = yup.object().shape({
	text: yup.string().required(),
	isCompleted: yup.boolean().required(),
});
```

## Create a server using express and then start it to send request

```
const app = express();

app.listen(3000);

```

## Create Validators for id and objects and an Error Handler to catch errors and send the necessary messages

## Then use the following functions:

1. get/todos: To receive the Todos table
2. get/todos/:id: To receive the information of a particular todo from its id
3. post/todos/:id: To add information in the Todos table
4. put/todos/:id: To update a particular todo
5. delete/todos/:id: to delete a particular todo
